import unittest
from unittest.mock import patch, MagicMock

from simple_pushover import (
    PushoverAPI,
    MessageCantBeFalseyError,
    ParameterAboveMaxLength,
)


class TestPushoverWrapper(unittest.TestCase):

    @patch("simple_pushover.get_env_value")
    def setUp(self, mock_get_env_value: MagicMock) -> None:
        mock_get_env_value.return_value = "YOUR_APP_TOKEN"
        self.api = PushoverAPI()

    def test_pushover_wrapper_setup(self):
        """Test that the PushoverAPI class is setup correctly."""
        self.assertEqual(
            self.api.push_endpoint, "https://api.pushover.net/1/messages.json"
        )
        self.assertEqual(self.api.token, "YOUR_APP_TOKEN")

    @patch("simple_pushover.requests.post")
    def test_pushover_wrapper_send_push_msg_success(self, mock_post: MagicMock) -> None:
        """Test the simple_send_push method success."""
        # set up example of a successful push
        mock_post.return_value.status_code = 200
        mock_post.return_value.json.return_value = {"status": 1}
        mock_post.return_value.ok = True
        result: bool = self.api.simple_send_push("Test message")
        self.assertTrue(mock_post.called)
        self.assertTrue(result)

    @patch("simple_pushover.requests.post")
    def test_pushover_wrapper_send_push_msg_fail_bad_response(
        self, mock_post: MagicMock
    ) -> None:
        """Test the simple_send_push method fail, bad response or invalid token."""
        # set up example of a successful push
        mock_post.return_value.status_code = 400
        mock_post.return_value.json.return_value = {"status": 0, "token": "invalid"}
        mock_post.return_value.ok = False
        result: bool = self.api.simple_send_push("Test message")
        self.assertTrue(mock_post.called)
        self.assertFalse(result)

    def test_pushover_wrapper_send_push_msg_fail_no_message(
        self,
    ) -> None:
        """Test the simple_send_push method fail, no message provided."""
        self.assertRaises(MessageCantBeFalseyError, self.api.simple_send_push, "")

    def test_pushover_wrapper_send_push_msg_fail_msg_too_long(
        self,
    ) -> None:
        """Test the simple_send_push method fail, message to long."""
        # 5 * 205 = 1025 characters
        self.assertRaises(
            ParameterAboveMaxLength, self.api.simple_send_push, "hyena" * 205
        )
